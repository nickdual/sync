require 'spec_helper'

describe YlBoatPicture do
  describe 'Picture' do
    it 'create' do
      boat = YlBoat.new({:name=>"Sunsail 33i", :supplier_guid=>"tui33S2K",  :description=>"Built to perform well under all conditions, this ya...", :hull=>"3.3", :length_ft=>"50", :cabins=>"2", :berths=>"?", :heads=>"1", :max_persons=>"6"})
      res = YlBoat.create(boat, 'tuisunsail', 'navigare_kastela')
      response_boat = JSON.parse(res.body)['boat']
      json =  [
          {'Boat Name' => 'Sunsail 33i', 'Boat Code' => "33S2K", 'Short Boat Description' => "Built to perform well under all conditions, this ya...", 'Beam' => "3.3", 'Sail area' => '50', "Cabins" => "2", "Berths" => "?", "Heads" => "1" ,"People (max)" => "6" ,'Layout Image URL' => "/image", 'Image 1' => "/image", 'Image 2' => "/image", 'Image 3' => "/image", 'Image 4' => "/image"},
          {'Boat Name' => 'Sunsail 42i/3/2', 'Boat Code' => "42S3K", 'Short Boat Description' => "The ultra - comfortable Sunsail 42i is designed in ...", 'Beam' => "4.11", 'Sail area' => '81', "Cabins" => "3", "Berths" => "?", "Heads" => "2", "People (max)" => "8" ,'Layout Image URL' => "/image", 'Image 1' => "/image", 'Image 2' => "/image", 'Image 3' => "/image", 'Image 4' => "/image"}
      ]

      value = YlBoatPicture.new({:url => "/image",:width => 208,:height => 68,:description => ''})
      boat_id = response_boat["id"]
      response = YlBoatPicture.create(boat_id, value)
      hash = JSON.parse(response.body)['boat_picture']
      hash.delete("id")
      hash.should == {"boat_id"=>  boat_id, "description"=>nil, "height"=>"68", "url"=>"/image", "width"=>"208"}
    end
  end
end

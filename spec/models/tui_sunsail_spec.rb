require 'spec_helper'

describe TuiBoat do
  describe "sync" do
    it "call _parse_local" do
      TuiBoat.should_receive(:_parse_response)
      TuiBoat.sync()
    end
    it "call sync" do
      TuiBoat.sync()
    end
    describe "parse response" do
      before(:each) do
        @boat_pictures = []
        @boat_pictures1 = YlBoatPicture.new({:url => "/image",:width => 208,:height => 68,:description => ''})
        @boat_pictures2 = YlBoatPicture.new({:url => "/image",:width => 208,:height => 68,:description => ''})
        @boat_pictures3 = YlBoatPicture.new({:url => "/image",:width => 208,:height => 68,:description => ''})
        @boat_pictures4 = YlBoatPicture.new({:url => "/image",:width => 208,:height => 68,:description => ''})
        @boat_pictures5 = YlBoatPicture.new({:url => "/image",:width => 208,:height => 68,:description => ''})
        @boat_pictures << @boat_pictures1 << @boat_pictures2 << @boat_pictures3 << @boat_pictures4 << @boat_pictures5
        @tui1 = YlBoat.new({:name=>"Sunsail 33i", :supplier_guid=>"tui33S2K",  :description=>"Built to perform well under all conditions, this ya...", :hull=>"3.3", :length_ft=>"50", :cabins=>"2", :berths=>"?", :heads=>"1", :max_persons=>"6"})
        @tui2 = YlBoat.new({:name=>"Sunsail 42i/3/2", :supplier_guid=>"tui42S3K", :description=>"The ultra - comfortable Sunsail 42i is designed in ...", :hull=>"4.11", :length_ft=>"81", :cabins=>"3", :berths=>"?", :heads=>"2", :max_persons=>"8"})
        @tui_boats1 = []
        @tui_boats2 = []
        @tui_boats1.push(:boat => @tui1, :boat_pictures => @boat_pictures)
        @tui_boats2.push(:boat => @tui2, :boat_pictures => @boat_pictures)
      end
      it "get expect result" do
        json =  [
            {'Boat Name' => 'Sunsail 33i', 'Boat Code' => "33S2K", 'Short Boat Description' => "Built to perform well under all conditions, this ya...", 'Beam' => "3.3", 'Sail area' => '50', "Cabins" => "2", "Berths" => "?", "Heads" => "1" ,"People (max)" => "6" ,'Layout Image URL' => "/image", 'Image 1' => "/image", 'Image 2' => "/image", 'Image 3' => "/image", 'Image 4' => "/image"},
            {'Boat Name' => 'Sunsail 42i/3/2', 'Boat Code' => "42S3K", 'Short Boat Description' => "The ultra - comfortable Sunsail 42i is designed in ...", 'Beam' => "4.11", 'Sail area' => '81', "Cabins" => "3", "Berths" => "?", "Heads" => "2", "People (max)" => "8" ,'Layout Image URL' => "/image", 'Image 1' => "/image", 'Image 2' => "/image", 'Image 3' => "/image", 'Image 4' => "/image"}
        ]
        datum = TuiBoat._parse_response(json)
        (datum[0].to_json.should  eq(@tui_boats1[0].to_json)) && (datum[1].to_json.should  == (@tui_boats2[0].to_json))
      end
    end
  end
end
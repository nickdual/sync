require 'spec_helper'

describe TuiBase do
  describe "sync" do
    it "call _parse_local" do
      TuiBase.should_receive(:_parse_response)
      TuiBase.sync()
    end
  end
  describe "parse response" do
    before(:each) do
      @tui1 = TuiBase.new(:id => 1, :name => "Shaton", :port => "3D2Y", :country => "Skypie")
      @tui2 = TuiBase.new(:id => 2, :name => "Shaton", :port => "3D2Y", :country => "Skypie")
    end
    it "get expect result" do
      json =  [
                {'loct' => 1, 'name' => "Shaton", 'port' => "3D2Y", 'country' => "Skypie"},
                {'loct' => 2, 'name' => "Shaton", 'port' => "3D2Y", 'country' => "Skypie"}
              ]
      datum = TuiBase._parse_response(json)
      datum[0].should == @tui1 && datum[1].should == @tui2
    end
  end
end
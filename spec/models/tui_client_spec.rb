require 'spec_helper'

describe TuiClient do
  describe 'get_location' do
    it 'get expect result' do
      excel = Roo::Excel.new(Rails.root.to_s + '/public/suppiler_marina.xls')
      datum = ApplicationHelper.xls_to_json(excel, [0, 5, 7, 22], ["loct", "name", "port", "country"])
      TuiClient.new().get_local().should == datum
    end
  end
end
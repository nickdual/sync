require 'spec_helper'

describe ApplicationHelper do
  describe 'parser xls to json' do
    context "pass suitable paramater" do
      before(:each) do
        @data = Roo::Excel.new(Rails.root.to_s + '/public/suppiler_marina.xls')
      end
      it "get 314 results" do
        response = ApplicationHelper.xls_to_json(@data)
        response.length.should == 314
      end
      it "get expect result" do
        data = ApplicationHelper.xls_to_json(@data, [0, 5, 7, 22], ["locl", "name", "port", "country"])[0]
        json = {"locl"=>"AJP", "name"=>"Porto V CR", "port"=>"", "country"=>"France"}
        data.should == json
      end
      context "with cols index" do
        it 'suitable' do
          data = ApplicationHelper.xls_to_json(@data, [0, 5, 7, 22])[0]
          data.should_not == nil && data.has_key?('BLLOCT').should == true && data.has_key?('BLNAME').should == true &&
              data.has_key?('BLPORT').should == true && data.has_key?('BLCTRY').should == true
        end
        it 'wrong value' do
          data = ApplicationHelper.xls_to_json(@data, [1])[0]
          data.should_not == nil && data.has_key?('BLLOCT').should == false
        end
      end
      context "with cols index and cols name" do
        it 'suitable' do
          data = ApplicationHelper.xls_to_json(@data, [0, 5, 7, 22], ["locl", "name", "port", "country"])[0]
          data.should_not == nil && data.has_key?('locl').should == true && data.has_key?('name').should == true &&
              data.has_key?('port').should == true && data.has_key?('country').should == true
        end
        it 'wrong value' do
          data = ApplicationHelper.xls_to_json(@data, [1], "loct")[0]
          data.should_not == nil && data.has_key?('BLLOCT').should == false
        end
      end
    end
  end
end
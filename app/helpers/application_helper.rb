module ApplicationHelper

	def	api_version
		Rails.logger.info "starting version call"
		response = Faraday.get 'http://127.0.0.1:3000/api/v1/version', {}, 'API-Key' => '003e43a433f27d2597627edda018c98e'
		Rails.logger.info response.body
		Rails.logger.info "end version"
	end

	#def	marina
	#	Rails.logger.info "starting marina call"
	#	response = Faraday.post 'http://127.0.0.1:3000/api/v1/supplier_marina', { :marina => { :name => "Maguro" } }, 'API-Key' => '003e43a433f27d2597627edda018c98e'
	#	Rails.logger.info response.body
	#	Rails.logger.info "end marina"
	#end

  def self.xls_to_json(sheet, cols=nil, colnames=nil, h=true)
    header = sheet.row(1)
    cols = Array.new(header.length){ |index| index } if cols.blank?
    results = []
    h ? row_start = 2 : row_start = 1
    (row_start..sheet.last_row).each do |i|
      row = sheet.row(i); row_modify = {}
      cols.each_with_index do |value, index|
        row_modify[%Q[#{colnames.blank? ? header[value] : colnames[index]}]] = row[value]
      end
      results.push(row_modify)
    end
    return results
  end
end

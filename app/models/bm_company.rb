require "rexml/document"
include REXML

class BmCompany < BmObject
  attr_accessor :name, :address, :city, :zip, :country, :telephone1, :telephone2,
                :fax1, :fax2, :mobile1, :mobile2, :vatcode, :email, :web

  @@companies = nil

  class << self
    # get all companies sorted by id
    def companies
      if not @@companies.nil?
        return @@companies
      end

      client = BmClient.new
      response_string = client.get_companies

      doc = REXML::Document.new response_string

      @@companies = []
      doc.elements.each("root/company") do |e|
        company = BmCompany.new(
            :id => e.attributes['id'],
            :name => e.attributes['name']
        )
        @@companies.push( company )
      end

      @@companies.sort! {|a,b| a.id.to_i <=> b.id.to_i }

      return @@companies
    end

    # find a company with the given id
    def find_one(id)
      companies = self.companies
      companies.each do |company|
        return company if company.id == id
      end

      return nil
    end

    # list every available companies to console
    def list
      companies = self.companies
      companies.each do |company|
        Rails.logger.info company.id + " - " + company.name if company and company.id and company.name
      end
    end

    # sync every company in config file
    def sync
      to_sync = APP_CONFIG['company_ids']
      to_sync.collect! {|x| x.to_s}

      to_sync.each do |company_id|
        company = self.find_one(company_id)

        if company.nil?
          Rails.logger.info "Company with id #{company_id} is not found"
          next
        end

        if YlPrincipal.find_by_supplier_guid(company.supplier_guid)
          Rails.logger.info "Company #{company.name} already exists"
          next
        end

        # sync company
        Rails.logger.info "Found company #{company.name} and sending to YL"
        response = YlPrincipal.create(company.name, company.supplier_guid)
        Rails.logger.info response
      end
    end
  end
end

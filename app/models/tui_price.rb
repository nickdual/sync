require 'net/ftp'

class TuiPrice < TuiObject
  class << self
    def sync
      bps = YlBoatPrice.list

      client = TuiClient.new
      client.get_remote_file('', '/tmp/', 'BROKEREURSS.CSV')
      return if !File.exist?('/tmp/BROKEREURSS.CSV')
      boat_prices = client.get_local_file('/tmp/BROKEREURSS.CSV', [0, 1, 2, 3, 4, 5, 6], ['suppiler_marina', 'boat_code', '', '', 'period_start', 'period_price'], false)
      boat_prices = self._parse_locations(boat_prices)

      bps.each do |server_price|
        exist = false
        boat_prices.each do |local_price|
          if server_price.boat_guid == 'tui'+local_price['boat_code'] &&
              server_price.period_start == Date.parse(local_price['period_start']) &&
              server_price.period_end == Date.parse(local_price['period_end']) &&
              server_price.period_price = local_price['period_price'].to_f
            exist = true
          end
        end
        if exist == false
          YlBoatPrice.destroy(server_price)
        end
      end

      boat_prices.each do |price|
        existing_price = YlBoatPrice.exist('tui'+price['suppiler_marina'], Date.parse(price['period_start']), Date.parse(price['period_end']))
        if existing_price
          if existing_price.period_price != price['period_price'].to_f
            existing_price.period_price = price['period_price']
            YlBoatPrice.update(existing_price)
          end
        else
          YlBoatPrice.create('tui'+price['suppiler_marina'], self._create_boat_price(price))
        end
      end
    end

    def _parse_locations(datum)
      locts = {}
      datum.each do |value|
        value['boat_code'] = 'tui' + value['boat_code'];
        value['suppiler_marina'] = 'tui' + value['suppiler_marina'];
        locts[value['boat_code']] = {} if locts[value['boat_code']].blank?
        locts[value['boat_code']][value['suppiler_marina']] = [] if locts[value['boat_code']][value['suppiler_marina']].blank?
        locts[value['boat_code']][value['suppiler_marina']].push(value)
      end
      normalizes = []
      locts.each do |iboat, boat|
        boat.each do |imarinas, marinas|
          next if marinas.length <= 1
          marinas[0..marinas.length - 2].each_with_index do |price, iprice|
            normalizes.push(price.merge({'period_end' => marinas[iprice+1]['period_start']}))
          end
        end
      end
      return normalizes
    end

    def _create_boat_price(data)
      params = {
          :boat_id => data['boat_id'],
          :boat_guid => data['boat_guid'],
          :period_price => data['period_price'],
          :period_nights => DateTime.parse(data['period_end']).mjd - DateTime.parse(data['period_start']).mjd,
          :period_start => data['period_start'],
          :period_end => data['period_end'],
          :calc => data['calc']
      }
      return YlBoatPrice.new(params)
    end
  end
end
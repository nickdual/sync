
class YlObject
  attr_accessor :id

  def initialize(attributes = {})
    attributes.keys.each do |key|
      self.send(key.to_s + '=', attributes[key])
    end
  end

  # create parameters ready to send to Yl API, ignore blank value
  def to_params
    hash = {}
    
    instance_variables.each do |sym|
      st = sym.to_s
      varname = st.slice(1, st.length) # remove @

      value = instance_variable_get(sym)
      if !value.blank?
        hash[varname.to_sym] = value
        #Rails.logger.info varname + ' ' + value.to_s
      end
    end

    return hash
  end
end

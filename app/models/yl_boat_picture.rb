require 'json'

class YlBoatPicture < YlObject
  attr_accessor :id, :boat_id, :url, :width, :height, :description

  class << self
    def create(boat_id, boat_pictures)
      params = {
          :boat_id => boat_id,
          :picture => boat_pictures.to_params,
      }
      client = YlClient.new
      response = client.post '/boat_pictures', params
      Rails.logger.info response.body
      return response
    end
  end
end

require 'json'

class YlSupplierMarina
  def self.list
    client = YlClient.new
    response = client.get('/supplier_marinas/list')
    return JSON.parse(response.body)['marinas']
  end

  def self.create(supplier_guid, name, address, country)
    client = YlClient.new
    params = {
      :supplier_marina => {
        :supplier_guid => supplier_guid,
        :name => name,
        :address => address,
        :country => country
      }
    }
    response = client.post('/supplier_marinas', params)
    return JSON.parse(response.body)
  end
end

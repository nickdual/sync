require 'json'

class YlCountry
  def self.list
    client = YlClient.new
    response = client.get('/countries/list')
    return JSON.parse(response.body)['countries']
  end
end


class YlPrincipal < YlObject
  attr_accessor :name, :supplier_guid

  @@principals = nil

  def initialize(id, name, supplier_guid)
    @id = id
    @name = name
    @supplier_guid = supplier_guid
  end

  class << self

    def principals
      return @@principals if not @@principals.nil?

      @@principals = self.list
    end

    def find_by_supplier_guid(supplier_guid)
      principals = self.principals
      principals.each do |principal|
        return principal if principal.supplier_guid == supplier_guid
      end

      return nil
    end

    ###############
    # API methods #
    ###############

    def list
      client = YlClient.new
      principals = []

      begin
        response = client.get '/principals/list'

        # parse
        x = JSON.parse(response.body)
        for p in x['principals'] do
          principal = YlPrincipal.new(p['id'], p['name'], p['supplier_guid'])
          principals.push(principal)
        end
      rescue Exception => e
        Rails.logger.info e.message
      end

      return principals
    end

    def create(name, supplier_guid)
      client = YlClient.new
      response = client.post '/principals', {:name => name, :supplier_guid => supplier_guid}
      return JSON.parse(response.body)
    end

  end
end

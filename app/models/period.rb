class Period
  attr_accessor :start, :end

  def initialize(period_start, period_end)
    @start = period_start
    @end = period_end
  end

  # check if the given period overlaps with this
  def overlap?(period)
    if ((period.start <= @start and @start < period.end)  or
        (period.start < @end and @end <= period.end) or
        (@start <= period.start and period.start < @start) or
        (@start < period.end and period.end <= @end))
      return true
    end

    return false
  end
end

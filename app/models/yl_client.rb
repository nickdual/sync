
class YlClient
  @api_url = APP_CONFIG['yl_client']['api_url']
  @api_key = ENV["YL_API_KEY"] || APP_CONFIG['yl_client']['api_key']
  class << self
    attr_reader :api_url, :api_key
    def initialize(api_url, api_key)
      @api_url = api_url
      @api_key = api_key
    end
  end

  def initialize
    @api_url = self.class.api_url
    @api_key = self.class.api_key
    @headers = {'API-Key' => @api_key}
  end

  def version
    return Faraday.get @api_url + '/version', {}, @headers
  end

  # newest faraday version will take @header as parameter not REQUEST HEADER
  # please pay attention in that
  def get(endpoint, params={})
    return Faraday.get @api_url + endpoint, params, @headers
  end

  def post(endpoint, params={})
    return Faraday.post @api_url + endpoint, params, @headers
  end

  def put(endpoint, params={})
    return Faraday.put @api_url + endpoint, params, @headers
  end

  def delete(endpoint, params={})
    return Faraday.delete @api_url + endpoint, params, @headers
  end
end

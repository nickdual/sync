require 'json'

class YlBoat < YlObject
  attr_accessor :id, :supplier_guid, :name, :model_name, :model_year, :description, :hull, :length_ft, :cabins, :berths, :heads, :max_persons, :fuel_capacity, :water_capacity

  class << self
    def list
      client = YlClient.new
      response = client.get '/boats/list'
      boats = self._parse_json(response.body)
      return boats
    end

    def list_by_principal_guid(principal_guid)
      client = YlClient.new
      response = client.get "/boats/list?principal_guid=#{principal_guid}"
      boats = self._parse_json(response.body)
      return boats
    end

    def list_by_boat_guid(boat_guid)
      client = YlClient.new
      response = client.get "/boats/list?boat_guid=#{boat_guid}"
      boats = self._parse_json(response.body)
      return boats
    end

    def create(boat, principal_guid, supplier_marina_guid)
      params = {
        :boat => boat.to_params,
        :principal_guid => principal_guid,
        :supplier_marina_guid => supplier_marina_guid
      }
      client = YlClient.new
      response = client.post '/boats', params
      Rails.logger.info response.body
      return response
    end

    def update(boat)
      client = YlClient.new
      #Rails.logger.info "id: #{boat_price.id}"
      response = client.put "/boat/#{boat_guid}", {
        :boat => boat.to_params.except(:id)
      }
      Rails.logger.info response.body
      return response
    end

    ###
    # Parse json string and return boats
    #
    # @param [String] json_string -- json-formatted string
    # @return [YlBoat] boats
    ###
    def _parse_json(json_string)
      boats = []
      #Rails.logger.info json_string
      x = JSON.parse(json_string)
      x['boats'].each do |x_boat|
        boat = YlBoat.new(x_boat)
        boats.push(boat)
      end
      
      return boats
    end
  end
end


class TuiObject
  attr_accessor :id
  def initialize(attributes = {})
    attributes.keys.each do |key|
      self.send(key.to_s + '=', attributes[key])
    end
  end
  def supplier_guid
    return 'tui' + @id
  end
end

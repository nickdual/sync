
class BmExtra < BmObject
  attr_accessor :boat
  attr_accessor :name,                    # [String]
                :category_name,           # [String]
                :price,                   # [Integer]
                :time_unit,               # [Integer] 0 - per booking, 1 - per day, 7 - per week
                :custom_quantity,         # [Integer] 1 - per person, 0 - per booking
                :valid_date_from,         # [Date]
                :valid_date_to,           # [Date]
                :sailing_date_from,       # [Date]
                :sailing_date_to,         # [Date]
                :obligatory,              # [Bool]
                :per_person,              # [Bool] whether the extra is charged per person or per booking
                :included_in_base_price   # [Bool]

  class << self
    ###
    # @param [REXML::Element] element
    # @return [BmExtra]
    ###
    def parse(element)
      extra = BmExtra.new
      extra.name = element.attributes['name']
      extra.category_name = element.attributes['categoryname']
      extra.price = element.attributes['price'].to_i
      extra.time_unit = element.attributes['timeunit'].to_i
      extra.custom_quantity = element.attributes['customquantity'].to_i
      extra.valid_date_from = Date.parse(element.attributes['validdatefrom'])
      extra.valid_date_to = Date.parse(element.attributes['validdateto'])
      extra.sailing_date_from = Date.parse(element.attributes['sailingdatefrom'])
      extra.sailing_date_to = Date.parse(element.attributes['sailingdateto'])
      extra.obligatory = (element.attributes['obligatory'] == '1') ? true : false
      extra.per_person = (element.attributes['perperson'] == '1') ? true : false
      extra.included_in_base_price = (element.attributes['includedinbaseprice'] == '1') ? true : false

      return extra
    end

    ###
    # Include mandatory extras computation for price
    #
    # This method update the given price object
    #
    # @param [YlPrice] price
    # @param [Array<BmExtra>] extras
    ###
    def compute_mandatory_extras(price, extras)
      total = 0

      extras = self.find(extras, price.period_start, price.period_end)
      extras.each do |extra|
        Rails.logger.info "Checking if " + extra.name + " for " + extra.price.to_s + " applies"
        
        is_match = false

        special_strings = ["outboard engine", "comfort", "cleaning", "tourist tax"]
        special_strings.each do |s|
          if extra.name.downcase.include?(s)
            is_match = true
          end
        end

        if !extra.nil? and !extra.included_in_base_price and extra.obligatory
          is_match = true
        end

        if is_match
        	Rails.logger.info "  Does apply!"
          if extra.per_person or extra.name.downcase.include?("per person")
            extra_price = extra.price * extra.boat.berths
          else
            extra_price = extra.price
          end

          if extra.time_unit == 1
            total += extra_price * 7
          else
            total += extra_price
          end
        end
      end

      return total
    end

    # find extras with the given period
    def find(extras, from, to)
      result = []
      extras.each do |extra|
        if from >= extra.valid_date_from and to <= extra.valid_date_to
          result.push(extra)
        end
      end

      return result
    end
  end
end

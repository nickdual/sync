
class BmClient
  def initialize
    @user_id = APP_CONFIG['bm_client']['user_id']
    @username = APP_CONFIG['bm_client']['username']
    @password = APP_CONFIG['bm_client']['password']
    @client = Savon.client do
      wsdl APP_CONFIG['bm_client']['service_url']
    end
    @base_params = [@user_id, @username, @password]
  end

  ###
  # WSDL endpoint: getBases
  #
  # @return response string
  ###
  def get_bases
    response = request('getBases')
    x = response.to_hash
    return x[:get_bases_response][:out]
  end

  ###
  # WSDL endpoint: getCompanies
  #
  # @return response string
  ###
  def get_companies
    response = request('getCompanies')
    x = response.to_hash
    return x[:get_companies_response][:out]
  end

  ###
  # WSDL endpoint: getResources
  #
  # @return response string
  ###
  def get_resources(company_id)
    response = request('getResources', [company_id])
    x = response.to_hash
    return x[:get_resources_response][:out]
  end

  def get_availability_info(company_id, year, only_busy, last_modified)
    response = request('getAvailabilityInfo', [company_id, year, only_busy, last_modified])
    x = response.to_hash
    return x[:get_availability_info_response][:out]
  end

  ###
  # raw request method, try to use wsdl method like get_bases instead
  #
  # @return [Savon::SOAP::Response]
  ###
  def request(endpoint, params=[], base_params=nil)
    base_params = @base_params if base_params.nil?
    params = base_params + params
    #Rails.logger.info "endpoint, params: #{endpoint.to_s} #{params.to_s}"
    endpoint = endpoint.underscore.parameterize('_').to_sym
    response = @client.call endpoint, message: _params_to_hash(params)
    return response
  end

  private

  def _params_to_hash(params)
    res = {}
    params.each_with_index do |value, i|
      res["in#{i.to_s}"] = value
    end
    return res
  end
end


class BmDiscount < BmObject
  attr_accessor :boat
  attr_accessor :name,                      # [String]
                :percentage,                # [Float] discount percentage - [0 - 100] %
                :valid_date_from,           # [Date]
                :valid_date_to,             # [Date]
                :sailing_date_from,         # [Date]
                :sailing_date_to,           # [Date]
                :valid_days_from,           # [Integer]
                :valid_days_to,             # [Integer]
                :discount_type,             # [Integer] 0 - duration discount, 1 - reservation period,
                                            #           2 - service period, 3 - last minute / early booking discount
                :excludes_other_discounts,  # [Bool]
                :included_in_base_price     # [Bool]

  class << self
    ###
    # @param [REXML::Element] element
    # @return [BmDiscount]
    ###
    def parse(element)
      discount = BmDiscount.new
      discount.name = element.attributes["name"]
      discount.percentage = element.attributes["percentage"].to_f
      discount.valid_days_from = element.attributes["validddaysfrom"].to_i
      discount.valid_days_to = element.attributes["validdaysto"].to_i
      discount.discount_type = element.attributes["discounttype"].to_i

      begin
        discount.valid_date_from = Date.parse(element.attributes["validdatefrom"])
      rescue
        discount.valid_date_from = nil
      end

      begin
        discount.valid_date_to = Date.parse(element.attributes["validdateto"])
      rescue
        discount.valid_date_to = nil
      end

      begin
        discount.sailing_date_from = Date.parse(element.attributes["sailingdatefrom"])
      rescue
        discount.sailing_date_from = nil
      end

      begin
        discount.sailing_date_to = Date.parse(element.attributes["sailingdateto"])
      rescue
        discount.sailing_date_to = nil
      end

      discount.excludes_other_discounts = (element.attributes["excludesotherdiscounts"] == "1")
      discount.included_in_base_price = (element.attributes["includedinbaseprice"] == "1")

      return discount
    end

    ###
    # @param [YlBoatPrice] price
    # @param [Array<BmDiscount>] discounts
    ###
    def compute_discounts(price, discounts)
      total = 0

      Rails.logger.info "Compute discounts on #{price.period_price}"

      valid_discounts = self.valid_discounts(discounts, price.period_start, price.period_end)
      exc_discount = exclusive_discount(valid_discounts)
      if !exc_discount.nil?
        Rails.logger.info "  Applied exclusive discount #{exc_discount.name} of #{price.period_price * (exc_discount.percentage / 100.0)} #{exc_discount.percentage}% on #{price.period_price}"
        return price.period_price * (exc_discount.percentage / 100.0)
      end

      valid_discounts.each do |d|
        total += price.period_price * (d.percentage / 100.0)
        Rails.logger.info "  Applied discount #{d.name} of #{price.period_price * (d.percentage / 100.0)}"
      end

      return total
    end

    # find exclusive discounts if there is one, nil otherwise
    def exclusive_discount(discounts)
      discounts.each do |discount|
        return discount if discount.excludes_other_discounts
      end

      return nil
    end

    ###
    # @param [Array<BmDiscount>] discounts
    # @param [Date] from
    # @param [Date] to
    ###
    def valid_discounts(discounts, from, to)
      result = []

      discounts.each do |d|
        next if d.included_in_base_price
        next if d.percentage < 0

        #if d.discount_type == 0     # duration discount
        #  duration = to - from
        #  if d.valid_days_from <= duration and duration <= d.valid_days_to
        #    result.push(d)
        #  end
        if d.discount_type == 1  # date of request
                                    # date of request in this case is date of sync
          request_date = Date.today
          if d.valid_date_from <= request_date and request_date < d.valid_date_to
            result.push(d)
          end
        elsif d.discount_type == 2  # date of sailing
          if d.sailing_date_from <= from and to <= d.sailing_date_to
            result.push(d)
          end
        elsif d.discount_type == 3  # last minute/early booking discount
                                    # date of request in this case is date of sync
          request_date = Date.today
          if !d.sailing_date_from.nil?
            date_from = d.sailing_date_from - d.valid_days_from
            date_to = d.sailing_date_from - d.valid_days_to
            #assert date_from < date_to
            if date_from <= request_date and request_date < date_to
              result.push(d)
            end
          end
        end
      end

      return result
    end
  end
end

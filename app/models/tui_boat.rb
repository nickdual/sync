require 'net/ftp'

class TuiBoat < TuiObject
  class << self
    def sync
      supplier_marinas = YlSupplierMarina.list()
      sm_guids = {}
      supplier_marinas.each {|sm| sm_guids[%Q[#{sm['supplier_guid']}]] = true} if supplier_marinas.present?

      client = TuiClient.new
      client.get_remote_file('', '/tmp/', 'BROKEREURSS.CSV')
      return if !File.exist?('/tmp/BROKEREURSS.CSV')
      dboat_prices = client.get_local_file('/tmp/BROKEREURSS.CSV', [0, 1, 2, 3, 4, 5, 6], ['suppiler_marina', 'boat_code', '', '', 'period_start', 'period_price'], false)
      dboat_prices.delete_if { |bl| sm_guids['tui'+ bl['suppiler_marina']].blank?}

      boat_locts = self._parse_locations(dboat_prices)
      boats = YlBoat.list_by_principal_guid('tuisunsail')
      bt_guids = []
      boats.each {|bt| bt_guids.push(bt.supplier_guid)} if boats.present?

      dboats = client.get_local_file(Rails.root.to_s + '/public/sunsail_yacht.csv')
      tui_boats = self._parse_boats(dboats)
      return if tui_boats.blank?
      for tui_boat in tui_boats do
        boat = tui_boat[:boat]
        if bt_guids.include?(boat.supplier_guid) || boat.model_name.blank? || boat_locts[boat.supplier_guid].blank?
          next
        end
        boat_locts[boat.supplier_guid].each do |index, prices|
          params = Marshal.load(Marshal.dump(boat.to_params))
          boat_clone = YlBoat.new([params, {:supplier_guid => params[:supplier_guid].insert(3, index[3..-1])}].inject(:merge))
          response = YlBoat.create(boat_clone, 'tuisunsail', index)

          prices.each do |price|
            YlBoatPrice.create(boat_clone.supplier_guid, self._create_boat_price(price))
          end

          tui_boat[:boat_pictures].each do |picture|
            YlBoatPicture.create(boat_clone.supplier_guid, picture)
          end
        end
      end
    end

    def _parse_locations(datum)
      locts = {}
      datum.each do |value|
        value['boat_code'] = 'tui' + value['boat_code'];
        value['suppiler_marina'] = 'tui' + value['suppiler_marina'];
        locts[value['boat_code']] = {} if locts[value['boat_code']].blank?
        locts[value['boat_code']][value['suppiler_marina']] = [] if locts[value['boat_code']][value['suppiler_marina']].blank?
        locts[value['boat_code']][value['suppiler_marina']].push(value)
      end
      normalizes = {}
      locts.each do |iboat, boat|
        normalizes[iboat] = {} if normalizes[iboat].blank?
        boat.each do |imarinas, marinas|
          normalizes[iboat][imarinas] = [] if normalizes[iboat][imarinas].blank?
          next if marinas.length <= 1
          marinas[0..marinas.length - 2].each_with_index do |price, iprice|
            normalizes[iboat][imarinas].push(price.merge({'period_end' => marinas[iprice+1]['period_start']}))
          end
        end
      end
      return normalizes
    end

    def _parse_boats(datum)
      tui_boats = []
      datum.each do |data|
        params = {
            :name => data['Boat Name'],
            :supplier_guid => 'tui' + data['Boat Code'],
            :model_name => data['Boat Name'],
            :model_year => '',
            :description => data['Short Boat Description'],
            :hull => 'mono',
            :length_ft => self.meters_to_feet(data['Length'].to_f).ceil,
            :cabins => data['Cabins'],
            :berths => (data['Berths'].blank? || data['Berths'] == '?') ? 6 : data['Berths'],
            :heads => data['Heads'],
            :max_persons => data['People (max)'],
            :fuel_capacity => '',
            :water_capacity => ''
        }
        boat = YlBoat.new( params )
        boat_pictures = []

        ['Layout Image URL', 'Image 1', 'Image 2', 'Image 3', 'Image 4'].each do |value|
          if data[value].present? && data[value].upcase != 'NULL' && data[value] != '?'
              boat_pictures.push(self._create_boat_picture(data[value]))
          end
        end
        tui_boats.push(:boat => boat, :boat_pictures => boat_pictures)
      end
      return tui_boats
    end

    def _create_boat_picture(link)
      params = {
          :url => link,
          :width => 208,
          :height => 68,
          :description => ''
      }
      return YlBoatPicture.new( params )
    end

    def _create_boat_price(data)
      params = {
        :boat_id => data['boat_id'],
        :boat_guid => data['boat_guid'],
        :period_price => data['period_price'],
        :period_nights => DateTime.parse(data['period_end']).mjd - DateTime.parse(data['period_start']).mjd,
        :period_start => data['period_start'],
        :period_end => data['period_end'],
        :calc => data['calc']
      }
      return YlBoatPrice.new(params)
    end

    def meters_to_feet(length)
      # 1 meter = 3.2808399 feet
      return length * 3.2808399
    end
  end
end
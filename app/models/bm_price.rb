
class BmPrice
  attr_accessor :boat, :from, :to, :price

  ###
  # @param [BmResource] boat
  # @param [String] from -- period from
  # @param [String] to -- period to
  # @param [Integer] price
  ###
  def initialize(boat, from, to, price)
    @boat = boat
    @from = Date.parse(from)
    @to = Date.parse(to)
    @price = price.to_i
  end

  # split price to period of 7 days and send to YL
  # NOTE that there isn't a check that deletes prices that don't exist at all! only deletes prices that are fully booked.
  def sync
    start = closest_upcoming_saturday(@from)
    stop = @to

    while start < stop do
      period = start + 7
      
     	Rails.logger.info "Syncing price (" + start.to_s + " " + period.to_s + "): " + price.to_s + " for boat " + boat.name

      if start > Date.today
        if self.is_blocked?(start, period)
          yl_price = YlBoatPrice.exist(@boat.supplier_guid, start, period)
          if yl_price
            # destroy
            Rails.logger.info "Destroy price #{@boat.supplier_guid}, #{yl_price.period_price}, #{yl_price.period_start}, #{yl_price.period_end}"
            YlBoatPrice.destroy(yl_price)
          end
        else
          existing_price = YlBoatPrice.exist(@boat.supplier_guid, start, period)
          if existing_price
            yl_price = YlBoatPrice.new(
                :period_price => @price,
                :period_nights => (period-start).to_i,
                :period_start => start,
                :period_end => period
            )
            apply_discount_and_extra(yl_price)
            if yl_price.period_price == existing_price.period_price
              # ignore, nothing to update
            else
              # update
              Rails.logger.info "Update price #{@boat.supplier_guid}, #{yl_price.period_price}, #{yl_price.period_start}, #{yl_price.period_end}"
              yl_price.id = existing_price.id
              YlBoatPrice.update(yl_price)
            end
          else
            yl_price = YlBoatPrice.new(
                :period_price => @price,
                :period_nights => (period-start).to_i,
                :period_start => start,
                :period_end => period
            )
            apply_discount_and_extra(yl_price)
            Rails.logger.info "Create price #{@boat.supplier_guid}, #{yl_price.period_price}, #{yl_price.period_start}, #{yl_price.period_end}"
            YlBoatPrice.create(@boat.supplier_guid, yl_price)
          end
        end
      end

      start = period
    end
  end

  def closest_upcoming_saturday(date)
    days_to_saturday = date.wday == 6 ? 0 : 6 - date.wday
    date + days_to_saturday
  end

  ###
  # @param [YlBoatPrice] yl_price
  ###
  def apply_discount_and_extra(yl_price)
    total_extra = BmExtra.compute_mandatory_extras(yl_price, @boat.extras)
    total_discount = BmDiscount.compute_discounts(yl_price, @boat.discounts)

  	calc = "Base price #{@price}"
  	if total_extra > 0 then calc << " Extras #{total_extra}" end
  	if total_discount > 0 then calc << " Discount #{total_discount}" end
  	yl_price.calc = calc

    Rails.logger.info "Base price, total extra, total discount: #{@price}, #{total_extra}, #{total_discount}"
    yl_price.period_price = @price + total_extra - total_discount
    yl_price.period_price = yl_price.period_price.ceil
  end

  def is_blocked?(pstart, pend)
    if @boat.blocked
      @boat.blocked.each do |blocked_period|
        # check that the two period overlaps
        if blocked_period.overlap?(Period.new(pstart, pend))
          return true
        end
      end
    end
    return false
  end
end

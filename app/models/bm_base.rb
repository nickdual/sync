require 'rexml/document'
require 'json'
include REXML

class BmBase < BmObject
  attr_accessor :name, :city, :country, :address

  def ==(base)
    return false if !base.instance_of?(BmBase)

    return (@id.eql?(base.id) and @name.eql?(base.name) and @city.eql?(base.city) and
            @country.eql?(base.country) and @address.eql?(base.address))
  end

  class << self
    def sync
      client = BmClient.new
      response_string = client.get_bases

      bm_bases = self._parse_response(response_string)

      # build list of marina supplier_guid
      supplier_marinas = YlSupplierMarina.list
      sm_guids = []
      supplier_marinas.each do |sm|
        sm_guids.push(sm['supplier_guid'])
      end

      # sync to yl
      for bm_base in bm_bases do
        if sm_guids.include?(bm_base.supplier_guid)
          next
        end

        Rails.logger.info bm_base.name

        response = YlSupplierMarina.create(bm_base.supplier_guid, bm_base.name,
                                           bm_base.address, bm_base.country)

        Rails.logger.info response
      end
    end

    def _parse_response(response_string)
      doc = Document.new response_string

      bm_bases = []

      doc.elements.each("root/base") do |e|
        params = {
            :id => e.attributes['id'],
            :name => e.attributes['name'],
            :city => e.attributes['city'],
            :country => e.attributes['country'],
            :address => e.attributes['address']
        }
        bm_base = self.new( params )
        bm_bases.push(bm_base)
      end

      return bm_bases
    end
  end
end

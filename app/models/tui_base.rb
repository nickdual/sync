
class TuiBase < TuiObject
  attr_accessor :name, :port, :country
  def ==(base)
    return false if !base.instance_of?(TuiBase)
    return (@id.eql?(base.id) and @name.eql?(base.name) and @port.eql?(base.port) and @country.eql?(base.country))
  end
  class << self
    def sync
      raw_countries = YlCountry.list()
      countries = {}
      if raw_countries.present?
        raw_countries.each do |country|
          countries[%Q[#{country['name'].downcase}]] = country['isocode']
        end
      end

      supplier_marinas = YlSupplierMarina.list()

      sm_guids = []
      if supplier_marinas.present?
        supplier_marinas.each do |sm|
          sm_guids.push(sm['supplier_guid'])
        end
      end
      client = TuiClient.new
      response_json = client.get_local_file(Rails.root.to_s + '/public/suppiler_marina.xls', [0, 5, 7, 22], ['loct', 'name', 'port', 'country'])
      tui_bases = self._parse_response(response_json)
      return if tui_bases.blank?
      for tui_base in tui_bases do
        if sm_guids.include?(tui_base.supplier_guid) ||
            (tui_base.id.blank? || tui_base.name.blank? || tui_base.port.blank? || tui_base.country.blank?)
          next
        end
        country = countries[%Q[#{tui_base.country.downcase}]]
        response = YlSupplierMarina.create(tui_base.supplier_guid, tui_base.name+' / '+tui_base.port, '',  country) if country.present?
        Rails.logger.info response
      end
    end

    def _parse_response(datum)
      tui_bases = []
      datum.each do |data|
        params = {
            :id => data['loct'],
            :name => data['name'],
            :port => data['port'],
            :country => data['country'],
        }
        tui_base = self.new( params )
        tui_bases.push(tui_base)
      end
      return tui_bases
    end
  end
end

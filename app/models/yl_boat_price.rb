require 'json'

class YlBoatPrice < YlObject
  attr_accessor :id, :boat_id, :boat_guid, :period_price, :period_nights, :period_start, :period_end, :calc

  @@prices = {}

  class << self
    def list
      client = YlClient.new
      response = client.get '/boat_avail_prices/list'
      boat_prices = self._parse_json(response.body)
      return boat_prices
    end

    def list_by_boat_id(boat_id)
      client = YlClient.new
      response = client.get "/boat_avail_prices/list?boat_id=#{boat_id}"
      boat_prices = self._parse_json(response.body)
      return boat_prices
    end

    def list_by_boat_guid(boat_guid)
      client = YlClient.new
      response = client.get "/boat_avail_prices/list?boat_guid=#{boat_guid}"
      boat_prices = self._parse_json(response.body)
      return boat_prices
    end

    def create(boat_guid, boat_price)
      client = YlClient.new
      response = client.post '/boat_avail_prices', {
        :boat_guid => boat_guid,
        :avail_boat => boat_price.to_params
      }
      Rails.logger.info boat_price.to_params
      Rails.logger.info response.body
      return response
    end

    def update(boat_price)
      client = YlClient.new
      #Rails.logger.info "id: #{boat_price.id}"
      response = client.put "/boat_avail_prices/#{boat_price.id}", {
        :avail_boat => boat_price.to_params.except(:id, :boat_guid)
      }
      Rails.logger.info response.body
      return response
    end

    def destroy(boat_price)
      client = YlClient.new
      response = client.delete "/boat_avail_prices/#{boat_price.id}"
      Rails.logger.info response.body
      return response
    end

    # this method will cache boat prices as long as the task is running
    def exist(boat_guid, period_start, period_end)
      if !@@prices.key? boat_guid
        @@prices[boat_guid] = self.list_by_boat_guid(boat_guid)
      end

      @@prices[boat_guid].each do |price|
        if price.boat_guid == boat_guid and price.period_start == period_start and price.period_end == period_end
          return price
        end
      end

      return nil 
    end

    def _parse_json(json_string)
      boat_prices = []

      x = JSON.parse(json_string)

      if x.has_key?('code') and x.has_key?('message')
        # error occured, return []
        return []
      end
      
      if x.has_key?('boats')
        x['boats'].each do |x_boat|
          bps = self._get_avail_prices(x_boat)
          boat_prices.concat(bps)
        end
      else
        boat_prices = self._get_avail_prices(x)
      end

      return boat_prices
    end

    def _get_avail_prices(boat_hash)
      #Rails.logger.info boat_hash
      
      boat_prices = []
      
      boat_id = boat_hash['boat_id']
      boat_guid = boat_hash['boat_guid']
      boat_hash['boat_avail_prices'].each do |price|
        boat_price = YlBoatPrice.new
        boat_price.id = price['id']
        boat_price.boat_id = boat_id
        boat_price.boat_guid = boat_guid
        boat_price.period_price = price['period_price'].to_i
        boat_price.period_nights = price['period_nights'].to_i
        boat_price.period_start = Date.parse(price['period_start_date'])
        boat_price.period_end = Date.parse(price['period_end_date'])

        boat_prices.push(boat_price)
      end

      return boat_prices
    end
  end
end


class TuiClient
  def initialize

  end

  def get_local_file(path, cols=nil, colnames=nil, h=true)
    data = open_spreadsheet(path)
    return ApplicationHelper.xls_to_json(data, cols, colnames, h)
  end

  def get_remote_file(path, dest, name)
    ftp = Net::FTP.new()
    ftp.passive = true
    ftp.connect(APP_CONFIG['fpt_server']['ip'])
    ftp.login(APP_CONFIG['fpt_server']['username'], APP_CONFIG['fpt_server']['password'])
    ftp.getbinaryfile(path + name, dest + name)
  end

  def open_spreadsheet(file)
    case File.extname(file).downcase
      when ".csv" then Roo::Csv.new(file, nil, :ignore)
      when ".xls" then Roo::Excel.new(file, nil, :ignore)
      when ".xlsx" then Roo::Excelx.new(file, nil, :ignore)
      else raise "Unknown file type: #{file}"
    end
  end
end

require "#{Rails.root}/config/initializers/load_config.rb"
require "#{Rails.root}/app/models/yl_client.rb"

namespace :tui do
  YlClient.initialize(APP_CONFIG['tui_client']['api_url'], APP_CONFIG['tui_client']['api_key'])

  desc "Sync everything"
  task :sync => :environment do
    TuiBase.sync
  end
  namespace :base do
    desc "Sync bases from TUI to YL"
    task :sync => :environment do
      TuiBase.sync
    end
  end
  namespace :boat do
    desc "Sync bases from TUI to YL"
    task :sync => :environment do
      TuiBoat.sync
    end
  end

  namespace :price do
    desc 'CRUD boat price'
    task :update => :environment do
      TuiPrice.sync
    end
  end
end

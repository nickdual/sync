require "#{Rails.root}/config/initializers/load_config.rb"
require "#{Rails.root}/app/models/yl_client.rb"

namespace :bm do
  YlClient.initialize(APP_CONFIG['bm_client']['api_url'], APP_CONFIG['bm_client']['api_key'])

  desc "Sync everything"
  task :sync => :environment do
    BmBase.sync
    BmCompany.sync
    BmResource.sync
    BmResource.sync_prices
  end

  namespace :base do
    desc "Sync bases from BM to YL"
    task :sync => :environment do
      BmBase.sync
    end
  end

  namespace :company do
    desc "Lists all companies in BM on command line"
    task :list => :environment do
      BmCompany.list
    end

    desc "Create company [sid] from BM into YL"
    task :create, [:sid] => :environment do |t, args|
      puts args[:sid]
      BmCompany.sync_one(args[:sid])
    end

    desc "Sync all companies in config file from BM into YL"
    task :sync => :environment do |t, args|
      BmCompany.sync
    end
  end

  namespace :boat do
    desc "Lists all boats for a company [sid] in BM on command line"
    task :list, [:sid] => :environment do |t, args|
      puts args[:sid]
      BmResource.list(args[:sid])
    end

    desc "Sync all boats for company listed on config file"
    task :sync => :environment do
      BmResource.sync
    end

    desc "Create all boats for company [sid] from BM into YL (no space between args!)"
    task :all_create, [:sid] => :environment do |t, args|
      puts args[:sid]
      BmResource.sync(args[:sid])
    end

    desc "Create boat [bid] for company [sid] from BM into YL (no space between args!)"
    task :create, [:sid, :bid] => :environment do |t, args|
      puts args[:bid] + " " + args[:sid]
      BmResource.sync(args[:sid], args[:bid])
    end
  end

  namespace :reservation do
    desc "Lists all boats reservation period for a company [sid] in BM on command line"
    task :list_all, [:sid] => :environment do |t, args|
      puts args[:sid]
      BmResource.list_booked_period(args[:sid])
    end

    desc "Lists all blocked availabilities for company [sid] and boat [bid] in BM on command line"
    task :list, [:sid, :bid] => :environment do |t, args|
      puts args[:sid] + " " + args[:bid]
      BmResource.list_booked_period(args[:sid], args[:bid])
    end
  end

  namespace :price do
    desc "List all prices" do |t, args|

    end

    desc "Update avail/prices for boat [bid] for company [sid] from BM into YL (no space between args!)"
    task :availprice, [:sid, :bid] => :environment do |t, args|
      puts args[:bid] + " " + args[:sid]
      BmResource.sync_prices(args[:sid], args[:bid])
    end

    desc "Update avail/prices for all boats for company [sid] from BM into YL (no space between args!)"
    task :all_availprice, [:sid] => :environment do |t, args|
      puts args[:sid]
      BmResource.sync_prices(args[:sid])
    end

    desc "Sync all boat prices for each company in config file"
    task :sync => :environment do
      BmResource.sync_prices
    end
  end

end

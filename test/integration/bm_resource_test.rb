require 'test_helper'

require File.dirname(__FILE__) + '/integration_base_test.rb'

class BmResourceTest < IntegrationBaseTest
  # these tests are made non-deterministic, because we don't want the test to run very long
  test "resources" do
    check_environment

    c = BmResourceTest.get_random_company

    resources = BmResource.resources(c.id)
    resources.each do |r|
      assert r.instance_of?(BmResource)
      assert !r.prices.nil?
      assert !r.images.nil?
    end
  end

  test "list" do
    check_environment

    c = BmResourceTest.get_random_company

    # redirect console output to string
    sio = StringIO.new
    old_stdout, $stdout = $stdout, sio

    BmResource.list(c.id)

    $stdout = old_stdout

    assert(sio.string.length > 0, 'output is empty')
  end

  test "list boat availability" do
    check_environment

    c = BmResourceTest.get_random_company

    # redirect console output to string
    sio = StringIO.new
    old_stdout, $stdout = $stdout, sio

    resources = BmResource.list_booked_period(c.id)
    resources.each do |r|
      assert r.instance_of?(BmResource)
      assert !r.prices.nil?
      assert !r.images.nil?
      assert !r.blocked.nil?
    end

    $stdout = old_stdout

    assert(sio.string.length > 0, 'output is empty')
  end

  private

  @@companies = nil

  def self.get_random_company
    if @@companies.nil?
      @@companies = BmCompany.companies
    end

    # pick one company at random
    lucky = rand(@@companies.length)
    c = @@companies[lucky]
    Rails.logger.debug 'Lucky company id: ' + c.id # tells us which company we choose, so we can debug it later
    return c
  end
end

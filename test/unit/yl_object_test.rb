require 'test_helper'

class YlObjectTest < ActiveSupport::TestCase
  test "to_hash method converts object to hash correctly" do
    params = {:id => 1, :a => 2, :b => 3, :c => 4}
    obj = YlDummyObject.new(params)
    assert_equal params, obj.to_params
  end
end

class YlDummyObject < YlObject
  attr_accessor :a, :b, :c
end

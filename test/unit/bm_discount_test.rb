require 'test_helper'

class BmDiscountTest < ActiveSupport::TestCase
  def setup
    @epoch = Date.parse("1970-1-1")
    @next_year = Date.today + 1.year
    @price = YlBoatPrice.new(
        :period_price => 1000,
        :period_nights => 7,
        :period_start => Date.today,
        :period_end => Date.today + 7
    )
  end

  def teardown
    @epoch = nil
    @next_year = nil
    @price = nil
  end

  #test "valid duration discount" do
  #  discounts = []
  #  discount1 = BmDiscount.new(
  #    :name => '1',
  #    :percentage => 10,
  #    :discount_type => 0,
  #    :valid_days_from => 5,
  #    :valid_days_to => 9
  #  )
  #  discounts.push(discount1)
  #
  #  t_discount = @price.period_price * (discount1.percentage / 100.0)
  #
  #  discount = BmDiscount.compute_discounts(@price, discounts)
  #
  #  assert_equal(t_discount, discount)
  #end
  #
  #test "invalid duration discount" do
  #  discounts = []
  #  discount1 = BmDiscount.new(
  #    :name => '1',
  #    :percentage => 10,
  #    :discount_type => 0,
  #    :valid_days_from => 14,
  #    :valid_days_to => 21
  #  )
  #  discount2 = BmDiscount.new(
  #    :name => '2',
  #    :percentage => -10,
  #    :discount_type => 0,
  #    :valid_days_from => 5,
  #    :valid_days_to => 9
  #  )
  #
  #  discounts.push(discount1)
  #  discounts.push(discount2)
  #
  #  discount = BmDiscount.compute_discounts(@price, discounts)
  #  assert_equal(0, discount)
  #end

  test "valid request date discount" do
    discounts = []
    discount1 = BmDiscount.new(
      :name => '1',
      :percentage => 10,
      :discount_type => 1,
      :valid_date_from => Date.today - 3,
      :valid_date_to => Date.today + 3
    )

    discounts.push(discount1)

    t_discount = @price.period_price * (discount1.percentage / 100.0)
    discount = BmDiscount.compute_discounts(@price, discounts)
    assert_equal(t_discount, discount)
  end

  test "invalid request date discount" do
    discounts = []
    discount1 = BmDiscount.new(
      :name => '1',
      :percentage => 10,
      :discount_type => 1,
      :valid_date_from => Date.today + 7,
      :valid_date_to => Date.today + 14
    )
    discount2 = BmDiscount.new(
        :name => '1',
        :percentage => -10,
        :discount_type => 1,
        :valid_date_from => Date.today - 3,
        :valid_date_to => Date.today + 3
    )

    discounts.push(discount1)
    discounts.push(discount2)

    discount = BmDiscount.compute_discounts(@price, discounts)
    assert_equal(0, discount)
  end

  test "valid sailing date discount" do
    discounts = []
    discount1 = BmDiscount.new(
      :name => '1',
      :percentage => 10,
      :discount_type => 2,
      :sailing_date_from => Date.today,
      :sailing_date_to => Date.today+7
    )

    discounts.push(discount1)

    t_discount = @price.period_price * (discount1.percentage / 100.0)
    discount = BmDiscount.compute_discounts(@price, discounts)
    assert_equal(t_discount, discount)
  end

  test "invalid sailing date discount" do
    discounts = []
    discount1 = BmDiscount.new(
      :name => '1',
      :percentage => 10,
      :discount_type => 2,
      :sailing_date_from => Date.today + 7,
      :sailing_date_to => Date.today + 14
    )
    discount2 = BmDiscount.new(
      :name => '2',
      :percentage => -10,
      :discount_type => 2,
      :sailing_date_from => Date.today,
      :sailing_date_to => Date.today + 7
    )

    discounts.push(discount1)
    discounts.push(discount2)

    discount = BmDiscount.compute_discounts(@price, discounts)
    assert_equal(0, discount)
  end

  test "valid last minute/early booking discount" do
    discounts = []
    discount1 = BmDiscount.new(
      :name => '1',
      :percentage => 10,
      :discount_type => 3,
      :sailing_date_from => Date.today + 14,
      :sailing_date_to => Date.today + 21,
      :valid_days_from => 17,
      :valid_days_to => 10
    )

    discounts.push(discount1)

    t_discount = @price.period_price * (discount1.percentage / 100.0)
    discount = BmDiscount.compute_discounts(@price, discounts)
    assert_equal(t_discount, discount)
  end

  test "invalid last minute/early booking discount" do
    discounts = []
    discount1 = BmDiscount.new(
      :name => '1',
      :percentage => -10,
      :discount_type => 3,
      :sailing_date_from => Date.today + 14,
      :sailing_date_to => Date.today + 21,
      :valid_days_from => 17,
      :valid_days_to => 10
    )
    discount2 = BmDiscount.new(
      :name => '2',
      :percentage => 10,
      :discount_type => 3,
      :sailing_date_from => Date.today + 14,
      :sailing_date_to => Date.today + 21,
      :valid_days_from => 10,
      :valid_days_to => 3
    )

    discounts.push(discount1)
    discounts.push(discount2)

    discount = BmDiscount.compute_discounts(@price, discounts)
    assert_equal(0, discount)
  end
end
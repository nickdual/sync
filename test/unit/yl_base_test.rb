require 'test_helper'

class YlBaseTest < ActiveSupport::TestCase
  # included for before_filter convenient method available on ActionController
  include AbstractController::Callbacks
  
  @@client = YlClient.new

  protected

  def check_yl_site_is_up
    begin
      response = @@client.version
      return true
    rescue Exception => e
      assert false, 'YL site is down'
      return false
    end
  end
end
